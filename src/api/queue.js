

module.exports = (app, options) => {
  const {repo} = options;
  // here we get all the movies 
  app.get('/queues', (request, response, next) => {
    repo.getAllQueues().then((queues) => {
        response.status(200).json(queues)
    }).catch(next);
  });
  
}