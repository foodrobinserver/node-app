const MongoClient = require('mongodb');
const config = require('./config/config');
// here we create the url connection string that the driver needs
// const getMongoURL = (options) => {
//   const url = options.servers
//     .reduce((prev, cur) => prev + `${cur.ip}:${cur.port},`, 'mongodb://')

//   return `${url.substr(0, url.length - 1)}/${options.db}`
// }

// mongoDB function to connect, open and authenticate
const connect = (options, mediator) => {
  mediator.once('boot.ready', () => {
    MongoClient.connect(`mongodb://${config.dbSettings.user}:${config.dbSettings.pass}@ds153593.mlab.com:53593/food`, { useNewUrlParser: true }, (error, client) => {
        if (error) {
          mediator.emit('db.error', error)
          return;
        }
        let db = client.db(`${config.dbSettings.db}`);
        mediator.emit('db.ready', db)
        // db.admin().authenticate(options.user, options.pass, (err, result) => {
        //   if (err) {
        //     mediator.emit('db.error', err)
        //   }
        //   mediator.emit('db.ready', db)
        // })
      });
  });
}

module.exports = Object.assign({}, {connect})