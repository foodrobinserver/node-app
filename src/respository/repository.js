
const repository = (db) => {
    // since this is the Queue-service, we already know
    // that we are going to query the `queues` collection
    // in all of our functions.
    const collection = db.collection('queues');

    const getAllQueues = () => {
        return new Promise((resolve, reject) => {
            collection.find({}).toArray().then((documents) => {
                resolve(documents);
            }).catch((error) => {
                reject(new Error('An error occured fetching all movies, err:' + error));
            });
        });
    }

    const disconnect = () => {
        db.close()
    }
    
    return Object.create({
        getAllQueues,
        disconnect
    });
}

const connect = (connection) => {
    return new Promise((resolve, reject) => {
        if (!connection) {
        reject(new Error('connection db not supplied!'));
        }
        resolve(repository(connection));
    });
}

// this only exports a connected repository
module.exports = {
    connect: connect
};
